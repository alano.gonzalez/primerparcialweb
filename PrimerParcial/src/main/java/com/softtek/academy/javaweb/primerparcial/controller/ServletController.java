package com.softtek.academy.javaweb.primerparcial.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.primerparcial.beans.ListBean;
import com.softtek.academy.javaweb.primerparcial.dao.ListSingletonDAO;

/**
 * Servlet implementation class ServletController
 */
public class ServletController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ListSingletonDAO lsd = ListSingletonDAO.getInstance();
		RequestDispatcher rd;
		String option = request.getParameter("option");
		if(option.equals("insert")) {
			String list = request.getParameter("List");
			ListBean lb = new ListBean(list);
			
			lsd.insertList(lb);
			 rd = request.getRequestDispatcher("Views/ToDoForm.jsp");
	         rd.forward(request, response);
		}
		if(option.equals("notDone")) {
			ArrayList<ListBean> notDone = lsd.getNotDoneList();
			request.setAttribute("notDone", notDone);
			rd = request.getRequestDispatcher("Views/NotDone.jsp");
			rd.forward(request, response);
			
		}
		if(option.equals("ListDone")) {
			int id = Integer.parseInt(request.getParameter("id"));
			lsd.ListDone(id);
			ArrayList<ListBean> notDone = lsd.getNotDoneList();
			request.setAttribute("notDone", notDone);
			rd = request.getRequestDispatcher("Views/NotDone.jsp");
			rd.forward(request, response);
		}
		if(option.equals("Done")) {
			ArrayList<ListBean> Done = lsd.getDoneList();
			request.setAttribute("Done", Done);
			rd = request.getRequestDispatcher("Views/Done.jsp");
			rd.forward(request, response);
		}
	}

}
