package com.softtek.academy.javaweb.primerparcial.beans;

public class ListBean {

	private String list;
	private boolean is_done;
	private int ID;
	
	
	public ListBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public ListBean(String list) {
		super();
		this.list = list;
		this.is_done = false;
	}
	
	public ListBean(String list, int ID) {
		this.list = list;
		this.ID = ID;
	}


	public ListBean(int ID,  String list, boolean is_done) {
		super();
		this.list = list;
		this.is_done = is_done;
		this.ID = ID;
	}


	public String getList() {
		return list;
	}


	public void setList(String list) {
		this.list = list;
	}


	public boolean isIs_done() {
		return is_done;
	}


	public void setIs_done(boolean is_done) {
		this.is_done = is_done;
	}


	public int getID() {
		return ID;
	}


	public void setID(int iD) {
		ID = iD;
	}
	
	
	
	
}
