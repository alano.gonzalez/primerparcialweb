package com.softtek.academy.javaweb.primerparcial.service;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.primerparcial.beans.ListBean;

public class ListService {
	
	public static Connection getConnection() {
		Connection con = null;
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/sesion3?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL, USER, PASS);
			
		}catch(SQLException e) {		
			e.printStackTrace();
			System.out.println(e);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return con;
	}
	
	public static void insertList(Connection con, ListBean lb) {
		 Connection con1 = con;
		 try {
			PreparedStatement ps = con1.prepareStatement("insert into to_do_list (list, is_done) values('" + lb.getList() + "', " + lb.isIs_done() + ");");
		ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static ArrayList<ListBean> getNotDone(Connection con) {
		Connection con1 = con;
		ArrayList<ListBean> notDone = new ArrayList<ListBean>();
		try {
			
			PreparedStatement ps = con1.prepareStatement("select * from to_do_list where is_done = false");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				notDone.add(new ListBean(rs.getString("list"), rs.getInt("id")));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return notDone;
	}
	
	public static void ListDone(Connection con, int id) {
		Connection con1 = con;
		int id1 = id;
		try {
			PreparedStatement ps = con1.prepareStatement("update to_do_list set is_done = true where id = " + id1 + ";");
		ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static ArrayList<ListBean> getDone(Connection con) {
		Connection con1 = con;
		ArrayList<ListBean> Done = new ArrayList<ListBean>();
		try {
			
			PreparedStatement ps = con1.prepareStatement("select * from to_do_list where is_done = true");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Done.add(new ListBean(rs.getString("list"), rs.getInt("id")));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return Done;
	}

}
