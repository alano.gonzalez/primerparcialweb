package com.softtek.academy.javaweb.primerparcial.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.primerparcial.beans.ListBean;
import com.softtek.academy.javaweb.primerparcial.service.ListService;


public class ListSingletonDAO {

	private static ListSingletonDAO uniqueInstance;
	private Connection connection;
	
	public static synchronized ListSingletonDAO getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new ListSingletonDAO();
		}
		return uniqueInstance;
	}
	
	private ListSingletonDAO() {
		super();
		this.connection = ListService.getConnection();
	}
	
	public void insertList(ListBean lb) {
		ListService.insertList(this.connection, lb);
	}
	
	public ArrayList<ListBean> getNotDoneList() {
		
		 return ListService.getNotDone(this.connection);
		
	}
	public void ListDone(int id){
		ListService.ListDone(this.connection, id);
	}
	
	public ArrayList<ListBean> getDoneList() {
		
		 return ListService.getDone(this.connection);
		
	}
	
	
}
