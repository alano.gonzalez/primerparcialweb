<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
    <%@ page import= "java.util.ArrayList" %>
    <%@ page import= "com.softtek.academy.javaweb.primerparcial.beans.ListBean" %>
    
    <% 
    		
    	String contextPath = request.getContextPath();
    	String title = "Done List";
    %>
<!DOCTYPE html>
<html>
<head>
<head>
	<title><%=title%></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
</head>
<body>
	
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="<%=contextPath%>">Primer Parcial</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="<%=contextPath%>/Views/ToDoForm.jsp">Add a new List</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Lists
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <form method="post" action="<%=contextPath%>/ServletController">
        	<input type="submit" style="border:none;" value="Go to My To Do List" class="dropdown-item">
        	<input type="hidden" name="option" value="notDone">
        </form>
        <form method="post" action="<%=contextPath%>/ServletController">
        	<input type="submit" style="border:none;" value="Go to My Done List" class="dropdown-item">
        	<input type="hidden" name="option" value="Done">
        </form>
        </div>
      </li>
    </ul>
  </div>
</nav>
	<br>
	<h2><%=title%></h2>
	<br><br>
<table style="width: 80%; text-align:center; margin:auto;" class="table table-hover">
  <thead class="thead-dark">
    <tr>
   	  <th scope="col">ID</th>
      <th scope="col">List</th>
    </tr>
  </thead>
  <tbody>
  <c:forEach var="list" items="${Done}">
   		<tr>
   			<td><c:out value = "${list.getID()}"/></td>
            <td><c:out value = "${list.getList()}"/></td>
        </tr>
  </c:forEach>

  </tbody>
</table>
	
</body>
</html>